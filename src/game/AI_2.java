package game;

import game.Desk.*;

public class AI_2 implements Strategy{
	//operation qu'il peut 
	public enum Oper{
		draw,use(new Object[3]),discard,sacrifice(new Object[1]);
		
		Object arg[];
		Oper(){
				this(null);
			}
		Oper(Object arg[]){
			this.arg=arg;
		}
	}
	
	@Override
	public void run(Game game, Player me) {
		game.showNow(me);
		
		Oper oper=null;
		Card cards[]=me.getCards();
		if(cards.length<=0) 
			oper=Oper.draw;
		else{
			for(int i=0;oper==null && i<cards.length;i++)
			{
				if(cards[i] instanceof Rescue_f3) 
					continue;
				//if sac
				if(cards[i] instanceof Believer_f1 || cards[i] instanceof Believer_f4)
					{
					oper=Oper.sacrifice;
					oper.arg[0]=i;
					continue;
					}
				for(Origin type:Origin.values())
					if(cards[i].canUse(me.actPoint,type))
					{	//if guide 
						if (cards[i] instanceof Archbishop)
						{	
							oper=Oper.use;
							oper.arg[0]= i;
							oper.arg[1]= type;
							for (int k=0;k<game.desk.center.size();k++)
								{
									if(Creed.get(cards[i].creed,game.desk.center.get(k).creed)>0)
									oper.arg[2] = k;	
								}
							break;
						}
					else
						{	oper=Oper.use;
							oper.arg[0]=i;
							oper.arg[1]=type;
							break;
						}
					}
			}
			if(oper==null) oper=Oper.discard;
			}
		System.out.print(">>>"+me.toString()+" "+(oper==null?"do nothing":oper.toString())+" ");
		
		if(oper==Oper.draw)
		{
			System.out.println(""+(7-cards.length)+" cards!");
			for(int i=cards.length;i<7;i++)
				me.draw();
		}
		else if(oper==Oper.use)
		{
			String s[]=null;
			Card card=cards[(int)(oper.arg[0])];
			System.out.print(card.toString());
			boolean flag=false;
			if(card instanceof Rescue)
			{
				if(card.use(me.actPoint,(Origin)oper.arg[1])
						&&game.useFunction(me,card,false)
						&&card.use(game,me,s))
					flag=true;
				} 
			else if (card instanceof Archbishop)
			{	
				if(card.use(me.actPoint,(Origin)oper.arg[1])
						&&card.use(game,me,(String)oper.arg[2]))
					flag=true;
			}
			else
				{
					if(game.useFunction(me,card,false)
							&&card.use(game,me,s))
						flag=true;
					}
				
			if(flag)
			{
				System.out.println(" success");
			}
			else System.out.println(" fail");
		}
		
		else if(oper==Oper.discard){
			System.out.println("");
			for(int i=0;i<cards.length;i++){
				me.discard(cards[i]);
				game.desk.discard(cards[i]);
			}
		}
		else if(oper==Oper.sacrifice)
		{

			String s[]=null;
			Card card=cards[(int)(oper.arg[0])];
			System.out.print(card.toString());
			boolean flag=false;
			if(game.useFunction(me,card,true)
					&&card.sacrifice(game, me))
					flag=true;
				
			if(flag)
			{
				System.out.println(" success");
			}
			else System.out.println(" fail");
		}
		else{
			System.out.println();
		}
	}

	@Override
	// carte ne function pas par DeuxEX (faire inutile)
	public boolean getWuxie(Game game, Player me, Player p,Card car) {
		return me!=p;
	}


}
